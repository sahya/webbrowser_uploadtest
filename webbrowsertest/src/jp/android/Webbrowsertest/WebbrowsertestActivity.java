package jp.android.Webbrowsertest;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;


public class WebbrowsertestActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final Button button = (Button)findViewById(R.id.GoButton);
        final OnClickListener listener = new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		final EditText edit = (EditText)findViewById(R.id.UriText);
        		final String uriString = edit.getText().toString();
        		final WebView web = (WebView)findViewById(R.id.web);
        		web.loadUrl(uriString);
        		web.requestFocus();
        	}
        };
        button.setOnClickListener(listener);
    }
}
	